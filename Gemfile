# basic
gem 'rails', '~>  5.2'
gem 'sassc-rails'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'jquery-rails', '~> 4.3'
gem 'turbolinks'
gem 'jbuilder', '~> 2.0'
gem 'sdoc', '~> 0.4.0', group: :doc

# rails 5.2 upgrades
#gem 'bootsnap', '>= 1.1.0', require: false # remove since it doesn't like read only containers
gem 'listen', '>= 3.0.5', '< 3.2'
gem 'puma', '~> 3.11'

# fastrails
gem 'bcrypt'
gem 'bootstrap', '~> 4.3'
gem 'font-awesome-sass'
gem 'simple_form'
gem 'devise'
gem 'gravatarify', '~> 3'
gem 'metamagic' # easily insert metatags for SEO / opengraph -> no updates in years
gem 'browser', '~> 2' # interface for telling us what browser is using the app, browser.modern? is 'outdated'
gem 'rails_12factor'
gem 'pg'
gem 'aws-sdk-s3' # configure in config/application.rb
gem 'fog-aws' # Requested to be added by carrierwave docs
gem 'carrierwave'
gem 'seed_dump' # import / export seed data from existing db (https://github.com/rroblak/seed_dump)
gem 'rename' # rails g rename:into new_app_name
gem 'rack-cors', :require => 'rack/cors'
gem 'httparty' # external HTTP requests, use rails c with httparty to play with data
gem 'activerecord-typedstore' # Typed app settings store-used for singleton
#gem 'ahoy_matey' # for analytics # https://github.com/ankane/ahoy
#gem 'vanity' # A/B testing

group :development do
  gem 'web-console', '~> 3'
  gem 'letter_opener' # view mailers in browser
end

group :development, :test do
  gem 'spring'
  gem 'foreman'
  gem 'better_errors'
  gem 'pry'
  gem 'awesome_print'
  gem 'binding_of_caller'
end

group :test do
  gem 'rspec-rails', '~> 3.8'
  gem 'spring-commands-rspec'
  gem 'factory_bot_rails', '~> 4'
  gem 'rails-controller-testing'
  gem 'chromedriver-helper'
  gem 'selenium-webdriver'
  gem 'capybara'
  gem 'database_cleaner'
  gem 'webmock'
  gem 'shoulda-callback-matchers', '~> 1.1.1'
  gem 'shoulda-matchers', '~> 3.1'
  gem 'faker'
end

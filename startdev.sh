#!/bin/bash

# Opens a new terminal tab
# Tested with gnome-terminal
WID=$(xprop -root | grep "_NET_ACTIVE_WINDOW(WINDOW)"| awk '{print $5}')
xdotool windowfocus $WID
xdotool key ctrl+t #ctrl+shift+t
wmctrl -i -a $WID
#  Opens a new browser window
xdg-open http://localhost:3000 &

# Install Gems
bundle install
# Migrate DB
rails db:migrate
rails s
